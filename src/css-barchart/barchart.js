const chartContainer = document.getElementById('chart');
let data;
function readJSON() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var response = JSON.parse(this.responseText);
      processData(response);
      data= response;
    }
  };
  xmlhttp.open("GET", "barchart.json", true);
  xmlhttp.send();
}

function processData(data) {
  let maxValueCount = 0;

  data.chartData.forEach((item, sindex) => {
    const bar = document.createElement('div');
    bar.className = 'bar';

    item.values.forEach((value, vindex) => {

      const detailsCount = item.details[vindex].length;

      const barSegment = document.createElement('div');
      barSegment.style.width = `${value}%`;
      barSegment.style.cursor= 'pointer';

      barSegment.setAttribute('data-serie', (sindex));
      barSegment.setAttribute('data-value', (vindex));
      barSegment.setAttribute('onclick', 'clickSerieValue(this)');

      barSegment.className = 'serie serie' + (vindex + 1);
      barSegment.textContent = `${value}% (${detailsCount})`;
      bar.appendChild(barSegment);
    });

    const barlegend = document.createElement('div');
    barlegend.className = 'barlegend';
    barlegend.textContent = item.series;

    //barlegend.appendChild(bar);

    chartContainer.appendChild(barlegend);
    chartContainer.appendChild(bar);

    
    
    /*
    const legendItem = document.createElement('div');
    legendItem.className = 'legend-item';
    
    const legendColor = document.createElement('span');
    legendColor.className = 'legend-color';
    legendColor.style.backgroundColor = `hsl(${index * 60}, 70%, 50%)`;
    */
  });

  let chartValueLegendContainer = document.getElementById('chartValueLegend');
  chartValueLegendContainer.innerText= "Legend";
  chartValueLegendContainer.className = 'chartValueLegend';
  
  const ul = document.createElement('ul');
    
  ul.className = 'square-list';
  
  data.chartValueDescriptions.forEach((value, index) => {
    
    const li = document.createElement('li');
    li.className = 'xbar';

    const square = document.createElement('div');

    square.className = 'square serie' + (index + 1);
    
    li.appendChild(square);
    const text = document.createElement('span');
    text.innerHTML = `${value}`;
    li.appendChild(text);
    ul.append(li);
    
  });
  chartValueLegendContainer.append(ul);
    
}


function detailtTable(dataDetails) {
  let chartDetailContainer = document.getElementById('chartDetail');
  
  const table = document.createElement('div');
  table.className = 'table';

  const headerRow = document.createElement('div');
  headerRow.className = 'row';

  if (dataDetails && dataDetails.length > 0) {
    for (const [key, value] of Object.entries(dataDetails[0])) {
      const cell = document.createElement('div');
      cell.textContent = `${key}`;
      cell.className = 'cell header';
      headerRow.appendChild(cell);
    };
    table.appendChild(headerRow);
  }



  dataDetails.forEach((item, sindex) => {
    const row = document.createElement('div');
    row.className = 'row';


    for (const [key, value] of Object.entries(item)) {
      const cell = document.createElement('div');
      cell.className = 'cell';
      cell.textContent = `${value}`;
      row.appendChild(cell);
    };

    table.appendChild(row);
  });

  chartDetailContainer.replaceChildren(table);
}

function clickSerieValue(element) {
  var details = data.chartData[element.getAttribute('data-serie')].details[element.getAttribute('data-value')];
  detailtTable(details);
}

readJSON();